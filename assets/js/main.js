class Utils {

  static setCookie = (name, value) => {

    document.cookie = `${name} = ${value}`;
  }
  
  static getCookieValueFor = (name) =>  {
  
    name = name + "=";

    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
  
    for(let index = 0, max = ca.length; index < max; index++) {
  
      let cookie = ca[index];
  
      while (cookie.charAt(0) == ' ') {
        cookie = cookie.substring(1);
      };
  
      if (cookie.indexOf(name) == 0) return cookie.substring(name.length, cookie.length);
    }
  
    return false;
  }
  
  static hasCookie = (name) =>  {
  
    const cookie = this.getCookieValueFor(name);
  
    if (cookie) return true;
  
    return false
  }
}

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}


// Settings are here
var total_items = 9999999999;
var start_minutes = 60 - 15; // 15 minutes
var start_secs = 1; // 15 minutes
var d = new Date();
var min_items_left = 12;
var max_items_left = 20;
var remaining_items = randomIntFromInterval(min_items_left, max_items_left);
var min_of_remaining_items = 1;
var decrease_after = 15;
var decrease_after_first_item = 1.77;

if( Utils.hasCookie('mins') ) {

  start_minutes = 59 - Utils.getCookieValueFor('mins');
}

if( Utils.hasCookie('secs') ) {

  start_secs = 60 - Utils.getCookieValueFor('secs');
}

// Davy Jones' Locker
(function ($) {
  $.fn.progressbar = function () {

    var b = this;

    setInterval(function () {

      let increase_sold = 1;
      $("#sold").html(Number($("#sold").text()) + increase_sold);
      remaining_items--;

      if (remaining_items < min_of_remaining_items) {
        remaining_items = randomIntFromInterval(min_items_left, max_items_left);
      }
    }, 1000 * decrease_after);
  };
})(jQuery);

jQuery.noConflict()(function ($) {
  $(document).ready(function () {
    $("#progress_bar").progressbar();
    
    var theMinsBox = $("#nummins");
    var theSecsBox = $("#numsecs");

    var gg = 0;
    var hh = 0;

    var nsec = 0 - start_secs;
    if (nsec < 0) {
      nsec = 60 - start_secs;
      gg = 1;
    }

    var nmin = 0 - start_minutes - gg;
    if (nmin < 0) {
      nmin = 60 - start_minutes - gg;
      hh = 1;
    }
    
    theSecsBox.html(nsec);
    theMinsBox.html(nmin);

    var refreshId = setInterval(function () {
      var e = theSecsBox.text();
      var a = theMinsBox.text();
      if (e == 0 && a == 0 && c == 0 && b == 0) {
      } else {
        if (e == 0 && a == 0 && c == 0) {
          theMinsBox.html("59");
          theSecsBox.html("59");
        } else {
          if (e == 0 && a == 0) {
            theMinsBox.html("59");
            theSecsBox.html("59");
          } else {
            if (e == 0) {
              theMinsBox.html(a - 1);
              theSecsBox.html("59");
            } else {
              theSecsBox.html(e - 1);
            }
          }
        }
      }

      let mins = Number($("#nummins").text());
      let secs = Number($("#numsecs").text());

      Utils.setCookie("mins", mins);
      Utils.setCookie("secs", secs);
    }, 1000);
  });
});
